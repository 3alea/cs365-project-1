﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamStabilizer : MonoBehaviour
{
    /* Set focus on target */
    public float yTarget;
    public float zTarget;
    Transform mTrans;

    // Start is called before the first frame update
    void Start()
    {
        mTrans = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        mTrans.LookAt(new Vector3(mTrans.position.x, yTarget, zTarget));
    }

    public void Shake(float duration, float magnitude)
    {
        StartCoroutine(ActualShake(duration, magnitude));
    }

    IEnumerator ActualShake(float duration, float magnitude)
    {
        Vector3 originalPos = transform.localPosition;

        float TimeEllpased = 0.0f;
        while (TimeEllpased < duration)
        {
            float x = Random.Range(-1.0f, 1.0f) * magnitude;
            float y = Random.Range(-1.0f, 1.0f) * magnitude;

            transform.localPosition = new Vector3(x, y, originalPos.z);

            TimeEllpased += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = originalPos;
    }
}
