﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// *********************************************************
// AUTHOR: Miguel Echeverria
// PURPOSE: Temporal player controller to be able to move
// the player freely with ASDW for testing.
// *********************************************************

public class ASDWMovement : MonoBehaviour
{
    // Rigidbody to move. Usually, the one of the player
    public Rigidbody rbToMove;

    // Speed at which the object will move
    public float Speed;

    // Keys used to move the object. Usually is ASDW.
    public KeyCode Left;
    public KeyCode Backwards;
    public KeyCode Right;
    public KeyCode Forward;

    
    void Start()
    {
        
    }


    void Update()
    {
        // Move to the left
        if (Input.GetKey(Left))
        {
            rbToMove.velocity += Vector3.left * Speed * Time.deltaTime;
        }

        // Move to the right
        if (Input.GetKey(Right))
        {
            rbToMove.velocity += Vector3.right * Speed * Time.deltaTime;
        }

        // Move forward
        if (Input.GetKey(Forward))
        {
            rbToMove.velocity += Vector3.forward * Speed * Time.deltaTime;
        }

        // Move backwards
        if (Input.GetKey(Backwards))
        {
            rbToMove.velocity += Vector3.back * Speed * Time.deltaTime;
        }
    }
}
