﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// **********************************************************************
// AUTHOR: Miguel Echeverria
// PURPOSE: It does the rotation effect of the coins, although it can
// be used for other objects.
// **********************************************************************


public class CoinLevitation : MonoBehaviour
{
    public float distance = 1;          // Distance the between the highest and lowest point
    public float time = 1;              // Time to get between the highest point and the lowest point
    public float startingDir = 1;       // Whether the object should start going up (1) or down (-1)

    private float timer;                // Timer for the lerp
    private float originalHeight;       // Starting height of the lerp
    private float endHeight;            // Final height for the lerp

    // Start is called before the first frame update
    void Start()
    {
        timer = 0.0f;
        originalHeight = transform.position.y + GetComponent<HazardMovement>().mInitialY;
        endHeight = originalHeight + distance * startingDir;
    }

    // Update is called once per frame
    void Update()
    {
        // If the timer has finished, reset it and change the beginning and end of the lerp
        if (timer > time)
        {
            timer = 0.0f;
            float temp = endHeight;
            endHeight = originalHeight;
            originalHeight = temp;
        }

        // Update the timer and compute the lerp
        timer += Time.deltaTime;

        float tn = timer / time;

        transform.position = new Vector3(transform.position.x, Mathf.SmoothStep(originalHeight, endHeight, tn), transform.position.z);
    }
}
