﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GlobalControl : MonoBehaviour
{
    [System.Serializable]
    public struct Stats
    {
        [SerializeField]
        public int lastScore;
        [SerializeField]
        public int highScore;
        [SerializeField]
        public int games;
    }

    public static GlobalControl Instance;

    public int score;
    public int lastScore;
    public int highscore;
    public int games;
    public bool inGame;

    void Start()
    {
        score = GlobalControl.Instance.score;
        lastScore = GlobalControl.Instance.lastScore;
        highscore = GlobalControl.Instance.highscore;
        games = GlobalControl.Instance.games;

        AudioManager mgr = FindObjectOfType<AudioManager>();
        if (!mgr.IsPlaying("Music"))
            FindObjectOfType<AudioManager>().Play("Music");
    }

    private void Update()
    {
        if (inGame)
        {
            inGame = false;
            games++;
            lastScore = 0;
        }
        score = PlayerStats.currentMatchCoins;

        if (score > highscore)
        {
            highscore = score;
        }
        if (score != 0)
        {
            lastScore = score;
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
            Deserialise();
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        //Since only the instance is serialised,
        //serialisation can occur at the destruction
        //of any stats.
        Serialise();
    }

    void Serialise()
    {
        // Create binary formatter and file destination
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/global.stats";
        FileStream file = new FileStream(path, FileMode.Create);

        Stats s = new Stats();
        s.games = Instance.games;
        s.highScore = Instance.highscore;
        s.lastScore = Instance.lastScore;

        // Pass data to serializor
        formatter.Serialize(file, s);

        file.Close();
    }

    void Deserialise()
    {
        // Get the file path to load from
        string path = Application.persistentDataPath + "/global.stats";
        if (File.Exists(path))
        {
            // Create binary formatter and get data
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream file = new FileStream(path, FileMode.Open);

            Stats data = (Stats) formatter.Deserialize(file);
            file.Close();

            //Takes the relevant data.
            Instance.games = data.games;
            Instance.highscore = data.highScore;
            Instance.lastScore = data.lastScore;

            return;
        }
        else
        {
            //File does not exist yet.

            //Debug.LogError("Save file not found in " + path);
            return;
        }
    }
}
