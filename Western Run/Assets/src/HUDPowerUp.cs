﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDPowerUp : MonoBehaviour
{
    private float duration = 0.0f;
    private float blinkDur;
    public float blinkTime;
    public float blinkDuration;
    public Color blinkColour;

    // Start is called before the first frame update
    void Start()
    {
        blinkDur = blinkDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if (duration <= 0.0f)
        {
            GetComponent<Image>().enabled = false;
            return;
        }

        duration -= Time.deltaTime;
        if(duration <= blinkTime)
        {
            blinkDur -= Time.deltaTime;
            if(blinkDur <= 0.0f)
            {
                blinkDur = blinkDuration;
                Image img = GetComponent<Image>();
                if (img.color == blinkColour)
                    img.color = Color.white;
                else
                    img.color = blinkColour;
            }
        }
    }

    public void SetDuration(float dur)
    {
        duration = dur;
        GetComponent<Image>().enabled = true;
        GetComponent<Image>().color = Color.white;
    }
}
