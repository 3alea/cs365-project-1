﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// **********************************************************************
// AUTHOR: Miguel Echeverria
// PURPOSE: Handle all the logic for the horse power up (picking it
// up, destroying it after certain time, setting the speed etc)
// **********************************************************************


public class HorsePowerUpLogic : MonoBehaviour
{
    // ----------Public static variables, these can be upgraded through the shop-----------------

    public static float duration;          // Duration of the horse power up
    private static HorsePowerUpLogic activeObject = null;
    public static bool active = false;
    private float timer;

    // ------------------------------------------------------------------------------------------

    [System.NonSerialized]
    public float horseSpeed = 0.85f;         // The speed the player will go when getting the horse (don't know if this can be upgraded)
    public float BaseSpeed;

    // Start is called before the first frame update
    void Start()
    {
        //Sets duration according to power up level.
        duration = 6 + PlayerStats.speedLevel * 2;

        //HazardMovement.mSpeed = BaseSpeed;
    }



    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDestroy()
    {
        if(active)
        {
            if(activeObject == this)
            {
                FinishPowerUp();
            }
        }
    }


    // Called once at the beginning of a collision of this object with another one
    // Use this instead of OnCollisionEnter because the power up acts only as a trigger.
    private void OnTriggerEnter(Collider other)
    {
        // If the power up has collided with the player, start the horse control coroutine
        // (which will change the speed, destroy the power up, control its lifetime etc)
        if (other.CompareTag("Player"))
        {
            /*!TODO
             * Powerup Pickup Sound
             */

            FindObjectOfType<AudioManager>().Play("PowerUpPick");

            if (!active)
            {
                //Notify that self is active.
                active = true;
                activeObject = this;
                //Start the coroutine.
                StartCoroutine(HorseControl());
            }
            else
            {
                //If active already exists, reset active's timer.
                activeObject.timer = duration;
                //Notify HUD of change.
                GameObject speed = GameObject.Find("HUD_Speed");
                speed.GetComponent<HUDPowerUp>().SetDuration(duration);
            }
        }
    }

    IEnumerator HorseControl()
    {
        //Disable other components.
        GetComponent<Collider>().enabled = false;
        GetComponent<HazardMovement>().enabled = false;
        GetComponent<CoinLevitation>().enabled = false;
        GetComponent<CoinRotation>().enabled = false;
        //Hide object.
        transform.position = new Vector3(0,-20,0);

        //Sets timer to duration.
        timer = duration;

        //Notifies HUD.
        GameObject speed = GameObject.Find("HUD_Speed");
        speed.GetComponent<HUDPowerUp>().SetDuration(duration);

        // Change the speed
        HazardMovement.mSpeed = horseSpeed;

        //Wait for timer.
        while(timer > 0.0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }

        FinishPowerUp();

        // Destroy the power up once its lifetime has finished
        GameObject.Destroy(gameObject);
    }

    private void FinishPowerUp()
    {
        //Deactivate.
        active = false;
        activeObject = null;

        // Reset the speed
        HazardMovement.mSpeed = BaseSpeed;
    }
}
