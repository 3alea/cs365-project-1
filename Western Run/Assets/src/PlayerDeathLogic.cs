﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PlayerDeathLogic : MonoBehaviour
{
    public string hazardTag;
    public GameObject gameOverGO;
    public GameObject cameraGO;
    public bool isDead;
    public string levelToLoad;
    public UnityEvent ShieldLost;

    bool ShieldAvailable = false;

    // Start is called before the first frame update
    void Start()
    {
        isDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == hazardTag)
        {
            if (ShieldAvailable)
            {
                ShieldAvailable = false;
                ShieldLost.Invoke();
                Destroy(collision.gameObject);
                return;
            }

            if (ShieldAvailable)
            {
                ShieldAvailable = false;
                ShieldLost.Invoke();
                Destroy(collision.gameObject);

                Debug.Log("Shaved xD");

                return;
            }

            Debug.Log("Not Saved");

            // Get rid of power-ups
            GameObject magnet = GameObject.Find("HUD_Magnet");
            if(magnet != null)
                magnet.GetComponent<HUDPowerUp>().SetDuration(0.0f);

            GameObject speed = GameObject.Find("HUD_Speed");
            if(speed != null)
                speed.GetComponent<HUDPowerUp>().SetDuration(0.0f);

            GameObject multi = GameObject.Find("HUD_Multiplier");
            if(multi)
                multi.GetComponent<HUDPowerUp>().SetDuration(0.0f);

            /*!TODO
             * Player Death Sound
             */

            FindObjectOfType<AudioManager>().Play("Hurt");

            isDead = true;
            Instantiate(gameOverGO, cameraGO.transform.position + 5.0f * cameraGO.transform.forward, cameraGO.transform.rotation);
        }
    }

    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    public void OnShieldPickUp()
    {
        ShieldAvailable = true;
    }
}
