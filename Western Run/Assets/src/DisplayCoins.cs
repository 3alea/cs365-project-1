﻿/*************************************************
 * File: DisplayCoins.cs
 * 
 * Purpose: Displays the player's total coins
 * 
 * Log: Created by Pablo Riesco at 28/05/2020
*************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCoins : MonoBehaviour
{
    // Reference to the object that stores the coins
    public GameObject player;

    PlayerStats stats;

    // Start is called before the first frame update
    void Start()
    {
        stats = player.GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Text>().text = "Coins: " + stats.coins;
    }
}
