﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// **********************************************************************
// AUTHOR: Miguel Echeverria
// PURPOSE: It does the rotation effect of the coins, although it
// can be used for other objects.
// **********************************************************************


public class CoinRotation : MonoBehaviour
{
    public float rotationAngle = 0.4f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Rotate around the y axis rotationAngle degrees per frame
        transform.Rotate(0, rotationAngle, 0);
    }
}
