﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDetails : MonoBehaviour
{
    // Reference to the GameObject that stores the player stats
    public GameObject player;

    PlayerStats stats;
    
    // Determines which type of button it is
    public enum upgrade { eSecondChance, eMagnet, eShield, eMultiplyer, eMount };
    public upgrade upgradeType;

    // Determines if it is a buy or sell button
    public bool typeBuySell;

    // List of prices to buy/sell
    public int[] priceBuySell = new int[5];

    // Start is called before the first frame update
    void Start()
    {
        stats = player.GetComponent<PlayerStats>();
    }

    // Updates every frame
    void Update()
    {
        // Update text
        updateText(upgradeType);
    }

    // Function to buy/sell stuff
    public void onClickEvent()
    {
        /*!TODO
        * Button Click Sound
        */

        FindObjectOfType<AudioManager>().Play("Button");

        // Custom behaviour when we are buying/selling
        if (typeBuySell == true)
        {
            // We are buying, so we need to check if the player has enough money for the upgrade
            // Cutom behaviour for every upgrade type
            switch (upgradeType)
            {
                // Second chance upgrade
                case upgrade.eSecondChance:
                    if (stats.secondChanceUpgrade < stats.maxPoints && stats.coins >= priceBuySell[stats.secondChanceUpgrade])
                    {
                        stats.coins -= priceBuySell[stats.secondChanceUpgrade];
                        stats.secondChanceUpgrade++;
                    }
                    break;

                // Magnet upgrade
                case upgrade.eMagnet:
                    if (stats.magnetUpgrade < stats.maxPoints && stats.coins >= priceBuySell[stats.magnetUpgrade])
                    {
                        stats.coins -= priceBuySell[stats.magnetUpgrade];
                        stats.magnetUpgrade++;
                    }
                    break;
                // Shield upgrade
                case upgrade.eShield:
                    if (stats.shieldUpgrade < stats.maxPoints && stats.coins >= priceBuySell[stats.shieldUpgrade])
                    {
                        stats.coins -= priceBuySell[stats.shieldUpgrade];
                        stats.shieldUpgrade++;
                    }
                    break;

                // Multiplyer upgrade
                case upgrade.eMultiplyer:
                    if (stats.coinMultiplierUpgrade < stats.maxPoints && stats.coins >= priceBuySell[stats.coinMultiplierUpgrade])
                    {
                        stats.coins -= priceBuySell[stats.coinMultiplierUpgrade];
                        stats.coinMultiplierUpgrade++;
                    }
                    break;

                // Mount upgrade
                case upgrade.eMount:
                    if (stats.mountDurationUpgrade < stats.maxPoints && stats.coins >= priceBuySell[stats.mountDurationUpgrade])
                    {
                        stats.coins -= priceBuySell[stats.mountDurationUpgrade];
                        stats.mountDurationUpgrade++;
                    }
                    break;
            }
        }
        else
        {
            // We are selling, so we need to check if the upgrade has at least one point
            // Cutom behaviour for every upgrade type
            switch (upgradeType)
            {
                // Second chance upgrade
                case upgrade.eSecondChance:
                    if (stats.secondChanceUpgrade > 0)
                    {
                        stats.secondChanceUpgrade--;
                        stats.coins += priceBuySell[stats.secondChanceUpgrade];
                    }
                    break;

                // Magnet upgrade
                case upgrade.eMagnet:
                    if (stats.magnetUpgrade > 0)
                    {
                        --stats.magnetUpgrade;
                        stats.coins += priceBuySell[stats.magnetUpgrade];
                    }
                    break;
                // Shield upgrade
                case upgrade.eShield:
                    if (stats.shieldUpgrade > 0)
                    {
                        stats.shieldUpgrade--;
                        stats.coins += priceBuySell[stats.shieldUpgrade];
                    }
                    break;

                // Multiplyer upgrade
                case upgrade.eMultiplyer:
                    if (stats.coinMultiplierUpgrade > 0)
                    {
                        --stats.coinMultiplierUpgrade;
                        stats.coins += priceBuySell[stats.coinMultiplierUpgrade];
                    }
                    break;

                // Mount upgrade
                case upgrade.eMount:
                    if (stats.mountDurationUpgrade > 0)
                    {
                        --stats.mountDurationUpgrade;
                        stats.coins += priceBuySell[stats.mountDurationUpgrade];
                    }
                    break;
            }
        }
    }

    void updateText(upgrade _upgrade)
    {
        if (typeBuySell == true)
        {
            switch (_upgrade)
            {
                // Second chance upgrade
                case upgrade.eSecondChance:
                    if(stats.secondChanceUpgrade >= stats.maxPoints)
                        this.GetComponentInChildren<Text>().text = "Upgrade";
                    else
                        this.GetComponentInChildren<Text>().text = "Upgrade (" + priceBuySell[stats.secondChanceUpgrade] + " coins)";
                    break;

                // Magnet upgrade
                case upgrade.eMagnet:
                    if (stats.magnetUpgrade >= stats.maxPoints)
                        this.GetComponentInChildren<Text>().text = "Upgrade";
                    else
                        this.GetComponentInChildren<Text>().text = "Upgrade (" + priceBuySell[stats.magnetUpgrade] + " coins)";
                    break;
                // Shield upgrade
                case upgrade.eShield:
                    if (stats.shieldUpgrade >= stats.maxPoints)
                        this.GetComponentInChildren<Text>().text = "Upgrade";
                    else
                        this.GetComponentInChildren<Text>().text = "Upgrade (" + priceBuySell[stats.shieldUpgrade] + " coins)";
                    break;
                // Multiplyer upgrade
                case upgrade.eMultiplyer:
                    if (stats.coinMultiplierUpgrade >= stats.maxPoints)
                        this.GetComponentInChildren<Text>().text = "Upgrade";
                    else
                        this.GetComponentInChildren<Text>().text = "Upgrade (" + priceBuySell[stats.coinMultiplierUpgrade] + " coins)";
                    break;
                // Mount upgrade
                case upgrade.eMount:
                    if (stats.mountDurationUpgrade >= stats.maxPoints)
                        this.GetComponentInChildren<Text>().text = "Upgrade";
                    else
                        this.GetComponentInChildren<Text>().text = "Upgrade (" + priceBuySell[stats.mountDurationUpgrade] + " coins)";
                    break;
            }
            
        }
        else
        {
            switch (_upgrade)
            {
                // Second chance upgrade
                case upgrade.eSecondChance:
                    if (stats.secondChanceUpgrade <= 0)
                        this.GetComponentInChildren<Text>().text = "Sell";
                    else
                        this.GetComponentInChildren<Text>().text = "Sell (" + priceBuySell[stats.secondChanceUpgrade - 1] + " coins)";
                    break;

                // Magnet upgrade
                case upgrade.eMagnet:
                    if (stats.magnetUpgrade <= 0)
                        this.GetComponentInChildren<Text>().text = "Sell";
                    else
                        this.GetComponentInChildren<Text>().text = "Sell (" + priceBuySell[stats.magnetUpgrade - 1] + " coins)";
                    break;

                // Shield upgrade
                case upgrade.eShield:
                    if (stats.shieldUpgrade <= 0)
                        this.GetComponentInChildren<Text>().text = "Sell";
                    else
                        this.GetComponentInChildren<Text>().text = "Sell (" + priceBuySell[stats.shieldUpgrade - 1] + " coins)";
                    break;

                // Multiplyer upgrade
                case upgrade.eMultiplyer:
                    if (stats.coinMultiplierUpgrade <= 0)
                        this.GetComponentInChildren<Text>().text = "Sell";
                    else
                        this.GetComponentInChildren<Text>().text = "Sell (" + priceBuySell[stats.coinMultiplierUpgrade - 1] + " coins)";
                    break;

                // Mount upgrade
                case upgrade.eMount:
                    if (stats.mountDurationUpgrade <= 0)
                        this.GetComponentInChildren<Text>().text = "Sell";
                    else
                        this.GetComponentInChildren<Text>().text = "Sell (" + priceBuySell[stats.mountDurationUpgrade - 1] + " coins)";
                    break;

            }
        }

    }

}
