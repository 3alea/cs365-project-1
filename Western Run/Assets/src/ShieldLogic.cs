﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldLogic : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        // If the power up has collided with the player, give it the magnet effect
        if (other.CompareTag("Player"))
        {
            /*!TODO
             * Powerup pickup SFX
             */

            FindObjectOfType<AudioManager>().Play("PowerUpPick");

            other.GetComponent<PlayerMovementLogic>().OnShieldPickUp(); 
            other.GetComponent<PlayerDeathLogic>().OnShieldPickUp(); 
        }
    }
}
