﻿using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;

public class BackgroundBehaviour : MonoBehaviour
{
    public static float frontZ = 60.0f;
    private GameObject playerGO;
    public Vector3 initialPos;

    // Start is called before the first frame update
    void Start()
    {
        playerGO = GameObject.Find("Player");
        initialPos = new Vector3(-496.7f, 0.01f, -68.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (playerGO.GetComponent<PlayerDeathLogic>().isDead)
            return;
        
        transform.position -= new Vector3(0.0f, 0.0f, 1.0f * Time.deltaTime * HazardMovement.GetSpeed() * 10); //move 

        if (gameObject.name != "Terrain")
        { 
            //Object loops around.
            if (transform.position.z < -10.0f)
                transform.position = new Vector3(transform.position.x, transform.position.y, frontZ);
        }

        if (gameObject.name == "Terrain")
        {
            if (transform.position.z < -990.0f)
                transform.position = initialPos;
        }
    }
}
