﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// **********************************************************************
// AUTHOR: Miguel Echeverria
// PURPOSE: Handle all the logic for the coin multiplier power up
// (picking it up, destroying it after certain time, setting the
// multiplier etc)
// **********************************************************************


public class CoinMultiplierLogic : MonoBehaviour
{
    // ----------Public static variables, these can be upgraded through the shop-----------------

    public static float duration = 7.5f;          // Duration of the multiplier power up
    public static int maxMultiplier = 2;        // The maximum coin multiplier (upgradeable through the shop)
    private static CoinMultiplierLogic activeObject = null;
    private static bool active = false;
    private float timer;

    // ------------------------------------------------------------------------------------------


    public static int currentMultiplier = 1;    // Coin multiplier (don't change this in the shop)


    // Start is called before the first frame update
    void Start()
    {
        duration = 7.5f + PlayerStats.magnetLevel * 2;
        maxMultiplier = 2; // PlayerStats.coinMultiplierLevel + 2;
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDestroy()
    {
        if (active)
        {
            if (activeObject == this)
            {
                //Deactivate.
                active = false;
                activeObject = null;

                //Reset the multiplier.
                currentMultiplier = 1;
            }
        }
    }


    // Called once at the beginning of a collision of this object with another one
    // Use this instead of OnCollisionEnter because the power up acts only as a trigger.
    private void OnTriggerEnter(Collider other)
    {
        // If the power up has collided with the player, start the multiplier control coroutine
        // (which will change the multiplier, destroy the power up, control its lifetime etc)
        if (other.CompareTag("Player"))
        {
            /*!TODO
             * Powerup Pickup Sound
             */


            FindObjectOfType<AudioManager>().Play("PowerUpPick");

            if (!active)
            {
                //Notify that self is active.
                active = true;
                activeObject = this;
                //Start the coroutine.
                StartCoroutine(MultiplierControl());
            }
            else
            {
                //If active already exists, reset active's timer.
                activeObject.timer = duration;
                //Notify HUD of change.
                GameObject multiplier = GameObject.Find("HUD_Multiplier");
                multiplier.GetComponent<HUDPowerUp>().SetDuration(duration);
            }
        }
    }

    IEnumerator MultiplierControl()
    {
        //Disable other components.
        GetComponent<Collider>().enabled = false;
        GetComponent<HazardMovement>().enabled = false;
        GetComponent<CoinLevitation>().enabled = false;
        GetComponent<CoinRotation>().enabled = false;
        //Hide object.
        transform.position = new Vector3(0, -20, 0);

        //Sets timer to duration.
        timer = duration;

        //Notifies HUD.
        GameObject multiplier = GameObject.Find("HUD_Multiplier");
        multiplier.GetComponent<HUDPowerUp>().SetDuration(duration);

        // Increase the multiplier
        currentMultiplier = maxMultiplier;

        // Wait until the power up has finished
        //Wait for timer.
        while (timer > 0.0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }

        //Deactivate.
        active = false;
        activeObject = null;

        // Reset the multiplier back to one
        currentMultiplier = 1;

        // Destroy the power up once its lifetime has finished
        GameObject.Destroy(gameObject);
    }
}
