﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// **********************************************************************
// AUTHOR: Miguel Echeverria
// PURPOSE: Handle all the logic of the coins (collision
// with player etc).
// **********************************************************************


public class CoinLogic : MonoBehaviour
{
    public float rotationAngleAfterCollision = 5;   // The rotation angle the coin will have after the player picked it up
    public float distUpAfterCollision = 3;          // Distance the coin will go up after colliding with the player
    public float timeToGoUp = 0.5f;                 // Time it takes for the coin to go up and be destroyed

    private bool collidedWithPlayer;                // Set to true after collision with player
    private float timer;                            // Timer for the interpolation
    private float initialHeight;                    // The height of the coin when the player collided with it
    private float finalHeight;                      // The final height of the coin, where it will be destroyed


    // Start is called before the first frame update
    void Start()
    {
        collidedWithPlayer = false;
        timer = 0.0f;

        initialHeight = transform.position.y;
        finalHeight = transform.position.y + distUpAfterCollision;
    }


    // Use LateUpdate instead of Update to avoid coin levitation resetting position
    private void LateUpdate()
    {
        // If the player has picked the coin
        if (collidedWithPlayer)
        {
            // Destroy the coin after the time has passed
            if (timer > timeToGoUp)
            {
                GameObject.Destroy(gameObject);
                timer = 0;
            }

            // Make the coin go up
            timer += Time.deltaTime;
            float tn = timer / timeToGoUp;

            transform.position = new Vector3(transform.position.x, Mathf.Lerp(initialHeight, finalHeight, tn), transform.position.z);
        }
    }


    // Called once at the beginning of a collision of this object with another one
    // Use this instead of OnCollisionEnter because the coins act only as triggers.
    private void OnTriggerEnter(Collider other)
    {
        // Play audio, increase coin stat, and start coin animation if other is the player
        if (other.CompareTag("Player"))
        {
            /*!TODO
             * Coin Pickup Sound
             */

            FindObjectOfType<AudioManager>().Play("Coin");

            gameObject.GetComponent<CoinRotation>().rotationAngle = rotationAngleAfterCollision;
            collidedWithPlayer = true;
            //gameObject.GetComponent<AudioSource>().Play();

            PlayerStats.currentMatchCoins += 1 * CoinMultiplierLogic.currentMultiplier;
        }
    }
}
