﻿/*************************************************
 * File: FromToBinary.cs
 * 
 * Purpose: Saves/loads to/from a binary file
 * 
 * Log: Created by Pablo Riesco at 28/05/2020
*************************************************/
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class FromToBinary
{
    /////////////
    // METHODS //
    /////////////
    // Used to save player stats data ONLY
    public static void SavePlayerStats (PlayerStats player)
    {
        // Create binary formatter and file destination
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerStats.dictatorpig";
        FileStream file = new FileStream(path, FileMode.Create);

        // Here we specify that we want to save player stats. We use the copy constructor to do so
        PlayerStatsData data = new PlayerStatsData(player);

        // Pass data to serializor
        formatter.Serialize(file, data);
        file.Close();
    }

    // Used to load player stats data ONLY
    public static PlayerStatsData LoadPlayerStats ()
    {
        // Get the file path to load from
        string path = Application.persistentDataPath + "/playerStats.dictatorpig";
        if(File.Exists(path))
        {
            // Create binary formatter and get data
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream file = new FileStream(path, FileMode.Open);

            PlayerStatsData data = formatter.Deserialize(file) as PlayerStatsData;
            file.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}
