﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointController : MonoBehaviour
{
    // Reference to the object that know the max amount of points
    public GameObject player;

    // Reference to the prefab to instantiate
    public GameObject prefab;

    // Determines which value to update
    public enum upgrade{eSecondChance, eMagnet, eShield, eMultiplyer, eMount};

    public upgrade upgradeType;

    // Difference between each point
    public int diff;

    // List of points
    public List<GameObject> points;

    PlayerStats stats;

    public int max;

    // Start is called before the first frame update
    void Start()
    {
        // Store the stats
        stats = player.GetComponent<PlayerStats>();

        max = stats.maxPoints;

        // Intantiate points acording to maxPoints
        for(int counter = 0; counter < max; counter++)
        {
            GameObject newPoint = Instantiate(prefab, new Vector3(counter * diff, 0, 0.1f), new Quaternion());
            newPoint.GetComponent<Toggle>().isOn = false;
            newPoint.transform.SetParent(this.transform, false);
            points.Add(newPoint);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Iterate through points and update if relative value is upgraded
        switch (upgradeType)
        {
            // Update by case
            case upgrade.eSecondChance:
                // Iterate through the list and update
                for(int counter = 0; counter < max; counter++)
                {
                    GameObject temp = points[counter];
                    // If the counter is less than the number of points, set to true
                    if (counter < stats.secondChanceUpgrade)
                        temp.GetComponent<Toggle>().isOn = true;
                    else
                        temp.GetComponent<Toggle>().isOn = false;
                }
                break;
            // Update by case
            case upgrade.eMagnet:
                // Iterate through the list and update
                for (int counter = 0; counter < max; counter++)
                {
                    GameObject temp = points[counter];
                    // If the counter is less than the number of points, set to true
                    if (counter < stats.magnetUpgrade)
                        temp.GetComponent<Toggle>().isOn = true;
                    else
                        temp.GetComponent<Toggle>().isOn = false;
                }
                break;
            // Update by case
            case upgrade.eShield:
                // Iterate through the list and update
                for (int counter = 0; counter < max; counter++)
                {
                    GameObject temp = points[counter];
                    // If the counter is less than the number of points, set to true
                    if (counter < stats.shieldUpgrade)
                        temp.GetComponent<Toggle>().isOn = true;
                    else
                        temp.GetComponent<Toggle>().isOn = false;
                }
                break;
            // Update by case
            case upgrade.eMultiplyer:
                // Iterate through the list and update
                for (int counter = 0; counter < max; counter++)
                {
                    GameObject temp = points[counter];
                    // If the counter is less than the number of points, set to true
                    if (counter < stats.coinMultiplierUpgrade)
                        temp.GetComponent<Toggle>().isOn = true;
                    else
                        temp.GetComponent<Toggle>().isOn = false;
                }
                break;
            // Update by case
            case upgrade.eMount:
                // Iterate through the list and update
                for (int counter = 0; counter < max; counter++)
                {
                    GameObject temp = points[counter];
                    // If the counter is less than the number of points, set to true
                    if (counter < stats.mountDurationUpgrade)
                        temp.GetComponent<Toggle>().isOn = true;
                    else
                        temp.GetComponent<Toggle>().isOn = false;
                }
                break;
        }
    }
}
