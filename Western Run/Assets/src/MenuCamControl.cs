﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCamControl : MonoBehaviour
{
    public float speedFactor = 0.05f;
    public float zoomFactor = 1.0f;
    public Transform currentMount;
    Vector3 lastPosition;

    private Camera cameraComp;

    void Start()
    {
        cameraComp = GetComponent<Camera>();

        lastPosition = transform.position;
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, currentMount.position, speedFactor * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, currentMount.rotation, speedFactor * Time.deltaTime);
        var velocity = Vector3.Magnitude(transform.position - lastPosition);
        cameraComp.fieldOfView = 60 + velocity * zoomFactor;
        lastPosition = transform.position;
    }
    public void setMount(Transform newMount)
    {
        currentMount = newMount;


        FindObjectOfType<AudioManager>().Play("Button2");

    }
}