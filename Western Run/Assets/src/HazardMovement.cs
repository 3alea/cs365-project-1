﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// HAZARD MOVEMENT
/// ---------------
/// AUTHOR IN PROTOTYPE: Ibai Abaunza
/// DESCRIPTION: Determine how a hazard behaves 
/// should be put on hazard object
/// </summary>
public class HazardMovement : MonoBehaviour
{
    public enum HazardType
    {
        Cactus, Totem, Eagle, Other
    };

    public enum BlockingType
    {
        eNone, eTop, eBottom, eFull
    };

    public GameObject playerGO;

    public HazardType mHtype; //variable to determine the hazard behavior

    public static float mSpeed = 0; //speed in which it moves forward
    public static float mMinSpeed = 40;
    public static float mMaxSpeed = 80;

    public static float TimeMaxSpeed = 50000;

    //EAGLE STUFF
    public float mEagleYOffset;  //offset of max y to lerp
    public float mEagleFlySpeed; //speed of lerp of eagle
    public float mInitialY;      //initial y of eagle
    public static float timee;
    public uint mMaxSpawnWeight = 1;
    public uint mAvailableSpawnWeight = 0;

    public BlockingType mBlockingType;
   

    // Start is called before the first frame update
    void Start()
    {
        playerGO = GameObject.Find("Player");

        mAvailableSpawnWeight = mMaxSpawnWeight;

        //initialize variables depending on type
        if (mHtype == HazardType.Cactus)
        {
            //Do Cactus stufff
            CactusInitialize();
        }
        else if (mHtype == HazardType.Eagle)
        {
            EagleInitialize();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerGO.GetComponent<PlayerDeathLogic>().isDead)
            return;

        //timee = playerGO.GetComponent<PlayerMovementLogic>().timee;

        transform.position -= new Vector3(0.0f, 0.0f, 1.0f * Time.deltaTime * GetSpeed() * 10); //move forward

        //Behavior depending on the type of enemy
        if (mHtype == HazardType.Cactus)
        {
            //Do Cactus stufff
            CactusBehavior();
        }
        else if (mHtype == HazardType.Eagle)
        {
            EagleBehavior();
        }

        //destroy object
        if (transform.position.z < -10.0f)
            Destroy(gameObject);
    }

    //Cactus behavior
    void CactusInitialize()
    {
        //put it above the road
        transform.position = new Vector3(transform.position.x, mInitialY, transform.position.z);
    }
    void CactusBehavior()
    {
        
    }

    //Eagle stuff 
    void EagleInitialize()
    {
        transform.position = new Vector3(transform.position.x, mInitialY, transform.position.z);
        mEagleYOffset = 0.5f;
    }
    void EagleBehavior()
    {
        float currentY = transform.position.y; //take current pos on y
        currentY = Mathf.Lerp(currentY, mInitialY + mEagleYOffset, mEagleFlySpeed); //lerp to the next position

        //change if next position is up or down
        float diff = (mInitialY + mEagleYOffset) - currentY;
        if (diff < 0.0f) diff *= -1.0f;

        //change dir if diference is small
        if(diff <= 0.001f)
        {
            mEagleYOffset = -mEagleYOffset; //change the offset
        }

        //change position
        transform.position = new Vector3(transform.position.x, currentY, transform.position.z);
    }

    public static float DiffCurve(float timePassed)
    {
        
        
        float t = timePassed * 555.555f;
        float v = (Mathf.Sqrt(t) / t * t * t) / (Mathf.Log((t + 1) + (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) +
                                            (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1)));

        v = Mathf.Clamp(v / 262583, 0, 1);
        if (float.IsNaN(v))
            return 0;
        return v;
    }

    public static float GetSpeed()
    {
        var playerGO = GameObject.Find("Player");

        return playerGO.GetComponent<PlayerMovementLogic>().GetSpeed();


    }
}
