﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// -----------------
/// HAZARDS GENERATOR
/// -----------------
/// STARTING PROGRAMMER: Ibai Abaunza
/// NEXT PROGRAMMER: Chris Kwakman
/// DESCRIPTION: This script should be put on the roadmanager (parent
/// obj of roads) and takes care of hazard spawning in the three roads
/// -----------------
/// </summary>
/// 

public class HazardGenerator : MonoBehaviour
{
    public GameObject playerGO;

    //Left, Right, and middle roads where hazards would be spawnned
    public Transform mLeftRoad;
    public Transform mMiddleRoad;
    public Transform mRightRoad;
    public List<GameObject> mHazards; //array of hazard prefabs (parameters on editor)

    //Timers for road spawning
    public float mMaxTimeLeft = 50;

    private float mTimer = 0.0f;
    private uint mTotalHazardWeight = 0;
    private uint mRemainingHazardWeight = 0;


    // Start is called before the first frame update
    void Start()
    {
        mTimer = 0.0f;

        foreach(GameObject hazard in mHazards)
        {
            HazardMovement hm = hazard.GetComponent<HazardMovement>();
            hm.mAvailableSpawnWeight = hm.mMaxSpawnWeight;
            mTotalHazardWeight += hm.mMaxSpawnWeight;
        }
        mRemainingHazardWeight = mTotalHazardWeight;

        spawn_hazard_pool = new List<HazardMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerGO.GetComponent<PlayerDeathLogic>().isDead)
            return;

        var f = playerGO.GetComponent<PlayerMovementLogic>();


        mTimer += Time.deltaTime * f.GetSpeed(); //update timers

       // print("Timer" + mTimer);

        //Choose a random enemy to spawn from the array
        if (mTimer > mMaxTimeLeft)
        {
            spawn_hazards();
            mTimer = 0.0f; //reset timer
        }
    }

    private List<HazardMovement> spawn_hazard_pool;

    /* \fn spawn_hazards
     * \brief Tries to create a valid configuration of three hazards, in such a way as to never block the player
     * from being able to make it through a row of obstacles, and to assure that hazards / powerups are spawned
     * at somewhat consistent intervals using a weighted spawning system.
     */
    private void spawn_hazards()
    {
        bool invalidConfiguration = true;
        spawn_hazard_pool.Clear();
        uint pickHazardAttempts = 0;
        // Loop until we have achieved a valid configuration of obstacles / powerups (hazards)
        while(invalidConfiguration)
        {
            while(spawn_hazard_pool.Count < 3)
            {
                int trySpawnHazardIndex = Random.Range(0, mHazards.Count);
                HazardMovement hm = mHazards[trySpawnHazardIndex].GetComponent<HazardMovement>();
                // If the spawning weight of this hazard is higher than zero, use it in this hazard row, and reduce its weight
                if (hm.mAvailableSpawnWeight > 0)
                {
                    spawn_hazard_pool.Add(hm);
                    hm.mAvailableSpawnWeight -= 1;
                    mRemainingHazardWeight -= 1;
                }
                // If all hazards have ran out of weight, reset hazard weights.
                else if (mRemainingHazardWeight == 0)
                    reset_hazard_weights();
                else
                    pickHazardAttempts++;
                if (pickHazardAttempts > 10)
                    reset_hazard_weights();
            }
            // Check if hazard configuration can actually be passed by player.
            invalidConfiguration = !are_hazards_passable();
            if (invalidConfiguration)
            {
                // If not, clear hazard spawning list and try again.
                spawn_hazard_pool.Clear();
                spawn_hazards();
                return;
            }
        }
        Object.Instantiate(spawn_hazard_pool[0].gameObject, mRightRoad.position, Quaternion.identity);
        Object.Instantiate(spawn_hazard_pool[1].gameObject, mMiddleRoad.position, Quaternion.identity);
        Object.Instantiate(spawn_hazard_pool[2].gameObject, mLeftRoad.position, Quaternion.identity);
        // Clear the pool of hazards that we just spawned in.
        spawn_hazard_pool.Clear();
    }

    private bool are_hazards_passable()
    {
        // Uses the blocking type assigned by the user to determine the "Collision Box" of said hazard.
        // This is used to guarantee that the player will be able to make it through a row of hazards.
        uint fully_blocked_lanes = 0;
        foreach(HazardMovement hazard in spawn_hazard_pool)
        {
            if (hazard.mBlockingType == HazardMovement.BlockingType.eFull)
                fully_blocked_lanes++;
        }
        return (fully_blocked_lanes < 3);
    }

    private void reset_hazard_weights()
    {
        // Reset the weights of all hazards so that they are allowed to spawn in again.
        mRemainingHazardWeight = mTotalHazardWeight;
        foreach(GameObject spawnableHazard in mHazards)
        {
            HazardMovement hm = spawnableHazard.GetComponent<HazardMovement>();
            hm.mAvailableSpawnWeight = hm.mMaxSpawnWeight;
        }
    }
}
