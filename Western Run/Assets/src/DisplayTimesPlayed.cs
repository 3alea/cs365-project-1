﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayTimesPlayed : MonoBehaviour
{
    GlobalControl inst;
    // Start is called before the first frame update
    void Start()
    {
        inst = GlobalControl.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        Text score = GetComponent<Text>();
        score.text = "Times Played: " + inst.games;
    }
}
