﻿/**********************************************************
 * File: PlayerMovementLogic.cs
 * 
 * Purpose: Basic Movement Controller for the player
 * 
 * Log: created by Alejandro Balea (05/26/2020)
 **********************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements.Experimental;
using UnityEngine.UI;

public class PlayerMovementLogic : MonoBehaviour
{
    // -+-+-+-+-+-+-+-+-+-+- Input -+-+-+-+-+-+-+-+-+-+- //
    public KeyCode LeftButton;
    public KeyCode RightButton;
    public KeyCode JumpButton;
    public KeyCode SlideButton;
    // -----------------------------------------------------


    // -+-+-+-+-+-+-+-+-+-+- Jumping -+-+-+-+-+-+-+-+-+-- //
    public float jumpForce;
    public CamStabilizer camController;
    // -------------------------------------------------- //
    public static float mMinSpeed = 1;
    public static float mMaxSpeed = 5;

    // -+-+-+-+-+-+-+-+-+-+- Sliding -+-+-+-+-+-+-+-+-+-+ // 
    public float slideIn;
    public float slideDuration;
    public float slideOut;
    float slideEllapsedTime;

    // bounding box scale when the player slides
    public float slidingScale;
    public float slideFactor;
    Vector3 slideScale;

    public ParticleSystem slidingParticles;
    ParticleSystem.MainModule particleSettings;

    public Color JumpNotReady;
    public Color JumpReady;
    // -----------------------------------------------------

    public GameObject ShieldObj;


    // Move lapsus from one position to another. The bigger the value,
    // the faster will the player move (Lerp) (Normalized)
    public float moveVariance;

    public string groundTag;

    // Ordered from left to right
    public List<Transform> Rows;

    // Current position of the player is in:
    // 0 = left
    // 1 = middle
    // 2 = right
    private int rowNum = 1;

    // x position of the row the player is in
    private float currentRow_x;


    // ------------------------- States -------------------------
    private bool isGrounded;
    private bool isSliding;

    bool StrongJumpReady = false;
    bool SlidingRequest = false;
    // ----------------------------------------------------------

    private Vector3 originalScale;

    // ----------------------- Components -----------------------
    private Transform tr;
    private Rigidbody rb;
    private BoxCollider bc;
    private PlayerDeathLogic deathLogic;
    // ----------------------------------------------------------

    public static float timee;

    // Start is called before the first frame update
    void Start()
    {
        timee = 0;
        tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        bc = GetComponent<BoxCollider>();
        deathLogic = GetComponent<PlayerDeathLogic>();

        OnShieldLost();

        originalScale = tr.localScale;
        slideScale = new Vector3(originalScale.x, originalScale.y * slidingScale, originalScale.z);
        slidingParticles.Pause();
        particleSettings = slidingParticles.main;
    }

    // Update is called once per frame
    void Update()
    {
        timee += Time.deltaTime;

        if (deathLogic.isDead)
            return;

        // Input: set position for current rows
        ManageInput();

        // Manage sliding state
        SlidingController();

        // Movement 
        float result = Mathf.Lerp(tr.localPosition.x, currentRow_x, moveVariance * Time.deltaTime);
        tr.localPosition = new Vector3(result, tr.localPosition.y, tr.localPosition.z);

        // Lock Rotation (except for y-axis)
        tr.rotation = Quaternion.Euler(0.0f, tr.rotation.y, 0.0f);
    }

   // private void OnCollisionEnter(Collision collision)
   // {
   //     if (collision.gameObject.CompareTag(groundTag) && !isGrounded)
   //         camController.Shake(0.1f, 0.01f);
   // }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag(groundTag)) 
            isGrounded = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag(groundTag))
            isGrounded = false;
    }

    private void ManageInput()
    {
        if (rowNum > 0 && Input.GetKeyDown(LeftButton))
            currentRow_x = Rows[--rowNum].localPosition.x;
        else if (rowNum < 2 && Input.GetKeyDown(RightButton))
            currentRow_x = Rows[++rowNum].localPosition.x;

        if (isGrounded && !isSliding)
        {
            if (Input.GetKeyDown(SlideButton) || SlidingRequest)
                InitSliding();

            if (Input.GetKeyDown(JumpButton))
                rb.AddForce(new Vector3(0.0f, jumpForce, 0.0f));
        }
        else if (isGrounded && isSliding)
        {
            if (Input.GetKeyDown(JumpButton))
            {
                EndSliding();

                if (StrongJumpReady)
                    rb.AddForce(new Vector3(0.0f, jumpForce * 1.4f, 0.0f));
                else
                    rb.AddForce(new Vector3(0.0f, jumpForce, 0.0f));
            }
        }
        else if (!isGrounded)
        {
            if (!SlidingRequest && Input.GetKeyDown(SlideButton))
            {
                /*!TODO
                 * Start Sliding Sound
                 */

                

                SlidingRequest = true;
                rb.AddForce(new Vector3(0.0f, -jumpForce, 0.0f));
            }
        }
    }


    private void SlidingController()
    {
        if (!isSliding) return;

        // Update the delta time
        slideEllapsedTime += Time.deltaTime * slideFactor * (HazardMovement.mSpeed * 2.0f + 0.9f);

        // Notice that it is an else if statement, it can only enter in one of them and
        // evaluates top to bottom, so when Ellapsed time is 0, it could enter on all of 
        // them, but the Slide In goes in first and rest of options are ignored

        if (slideEllapsedTime < slideIn)    // Effect of going down.
        {
            // Set the new size, from the original size to the sliding state
            float factor = slideEllapsedTime / slideIn;

            tr.localScale = Vector3.Lerp(originalScale, slideScale, factor);
            AdjustHeight();

            particleSettings.startColor = JumpNotReady;
        }
        else if (slideEllapsedTime < ( slideDuration + slideIn ))
        {
            // Make sure the sizes are consistent and there are no floating point errors
            tr.localScale = slideScale;
            AdjustHeight();

            float factor = (slideEllapsedTime - slideIn) / slideDuration;

            if (factor < 0.5)
                particleSettings.startColor = JumpNotReady;
            else
            {
                StrongJumpReady = true;
                particleSettings.startColor = JumpReady;
            }
        }
        else if (slideEllapsedTime < ( slideDuration + slideIn + slideOut ))
        {
            // Set the new size, from the sliding state to the original size
            float factor = (slideEllapsedTime - slideIn - slideDuration) / slideOut;
            
            tr.localScale = Vector3.Lerp(slideScale, originalScale, factor);
            AdjustHeight();

            particleSettings.startColor = JumpReady;
        }
        else
        {
            EndSliding();
        }
    }

    /* When Sliding, the boxcollider is adjusted so it fits the enviroment, however
     * if only the size is adjusted, it will spend some time in free fall state, 
     * being affected only by gravity, but actually we want to control that (both
     * for camera smoothness and gameplay) */
    private void AdjustHeight()
    {
        float newHeight = ((tr.localScale.y - 1.0f) / 2.0f) + 1.0f;
        tr.localPosition = new Vector3(tr.localPosition.x, newHeight, tr.localPosition.z);
    }

    private void InitSliding()
    {
        AudioManager tmp = FindObjectOfType<AudioManager>();
        
        if (!tmp.IsPlaying("Slide"))
            tmp.Play("Slide");
        
        isSliding = true;
        SlidingRequest = false;
        slideEllapsedTime = 0.0f;

        StrongJumpReady = false;
        slidingParticles.Play();
    }

    private void EndSliding()
    {
        // Make sure the sizes are consistent and there are no floating point errors
        tr.localScale = originalScale;
        AdjustHeight();

        // End the sliding process
        isSliding = false;

        slidingParticles.Clear();
        slidingParticles.Pause();

        FindObjectOfType<AudioManager>().Stop("Slide");
    }

    public void OnShieldPickUp()
    {
        ShieldObj.SetActive(true);
    }
    public void OnShieldLost()
    {
        ShieldObj.SetActive(false);
    }

    public static float DiffCurve(float timePassed)
    {
        float t = timePassed * 555.555f;
        float v = (Mathf.Sqrt(t) / t * t * t) / (Mathf.Log((t + 1) + (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) +
                                            (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) + (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1) * (t + 1)));

        v = Mathf.Clamp(v / 262583, 0, 1);
        if (float.IsNaN(v))
            return 0;
        return v;
    }

    public float GetSpeed()
    {

        GameObject speed = GameObject.Find("HUD_Speed");

        if(speed.GetComponent<Image>().enabled == false)
            return Mathf.Lerp(mMinSpeed, mMaxSpeed, DiffCurve(timee));
        else
            return 2 * Mathf.Lerp(mMinSpeed, mMaxSpeed, DiffCurve(timee));
    }

}
