﻿/*************************************************
 * File: PlayerStats.cs
 * 
 * Purpose: Stores the player's stats
 * 
 * Log: Created by Pablo Riesco at 28/05/2020
*************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Player stats, storing all of the player's stats throughout the game
[System.Serializable]
public class PlayerStatsData
{
    //////////////////////////
    // STATS STORED TO FILE //
    //////////////////////////
    // Money amount
    public int coins;

    // Second Chance upgrade
    public int secondChanceUpgrade;

    // Magnet upgrade
    public int magnetUpgrade;

    // Shield upgrade
    public int shieldUpgrade;

    // Coin Multiplier upgrade
    public int coinMultiplierUpgrade;

    // Mount Duration upgrade
    public int mountDurationUpgrade;

    // Max amount of points obtainable per upgrade
    public int maxPoints;

    /////////////
    // METHODS //
    /////////////
    // Copy constructor
    public PlayerStatsData(PlayerStats playerStats)
    {
        // Copy all of the stats from the one passed
        coins = playerStats.coins;
        secondChanceUpgrade = playerStats.secondChanceUpgrade;
        magnetUpgrade = playerStats.magnetUpgrade;
        shieldUpgrade = playerStats.shieldUpgrade;
        coinMultiplierUpgrade = playerStats.coinMultiplierUpgrade;
        mountDurationUpgrade = playerStats.mountDurationUpgrade;
        maxPoints = playerStats.maxPoints;
    }
}
