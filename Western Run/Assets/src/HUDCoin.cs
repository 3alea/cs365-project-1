﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDCoin : MonoBehaviour
{
    private Text textComp;
    // Start is called before the first frame update
    void Start()
    {
        textComp = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        textComp.text = "x" + PlayerStats.currentMatchCoins.ToString();
    }
}
