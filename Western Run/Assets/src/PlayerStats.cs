﻿/*************************************************
 * File: PlayerStats.cs
 * 
 * Purpose: Place where the player's upgrade stats and coins are
 * 
 * Log: Created by Pablo Riesco at 28/05/2020
*************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    // The coins the player has on the current match (not serialized)
    public static int currentMatchCoins = 0;
    public static int coinMultiplierLevel = 0;
    public static int magnetLevel = 0;
    public static int speedLevel = 0;

    //////////////////////////
    // STATS STORED TO FILE //
    //////////////////////////
    // Money amount
    public int coins;

    // Second Chance upgrade
    public int secondChanceUpgrade;

    // Magnet upgrade
    public int magnetUpgrade;

    // Shield upgrade
    public int shieldUpgrade;

    // Coin Multiplier upgrade
    public int coinMultiplierUpgrade;

    // Mount Duration upgrade
    public int mountDurationUpgrade;

    // Max amount of points obtainable per upgrade
    public int maxPoints;

    // Copy constructor
    public PlayerStats(PlayerStats playerStats)
    {
        // Copy all of the stats from the one passed
        coins = playerStats.coins;
     //   secondChanceUpgrade = playerStats.secondChanceUpgrade;
        magnetUpgrade = playerStats.magnetUpgrade;
        shieldUpgrade = playerStats.shieldUpgrade;
        coinMultiplierUpgrade = playerStats.coinMultiplierUpgrade;
        mountDurationUpgrade = playerStats.mountDurationUpgrade;
        maxPoints = playerStats.maxPoints;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Load
        LoadStats();
    }

    // Destructor, calling the save stats
    void OnDestroy()
    {
        // Save
        SaveStats();
    }

    // Reset function resetting the stats back to 0
    public void Reset()
    {
        coins = 0;
    //    secondChanceUpgrade = 0;
        magnetUpgrade = 0;
     //   shieldUpgrade = 0;
        coinMultiplierUpgrade = 0;
        mountDurationUpgrade = 0;
        maxPoints = 5;

        FindObjectOfType<AudioManager>().Play("Button");
    }

    ///////////////////
    // SERIALIZATION //
    ///////////////////
    // Used to save stats to file
    public void SaveStats()
    {
        //Saves upgrade to static.
        coinMultiplierLevel = coinMultiplierUpgrade;
        magnetLevel = magnetUpgrade;
        speedLevel = mountDurationUpgrade;

        FromToBinary.SavePlayerStats(this);

        AudioManager tmp = FindObjectOfType<AudioManager>();
        if(tmp != null)
            tmp.Play("Button");
    }

    // Used to load stats from file
    public void LoadStats()
    {
        // Load to a new PlayerStats
        PlayerStatsData temp = FromToBinary.LoadPlayerStats();

        if (temp == null)
        {
            SaveStats();
            temp = FromToBinary.LoadPlayerStats();
            if (temp == null)
                return;
        }

        // Copy values into this player stats
        coins = temp.coins += currentMatchCoins;
     //   secondChanceUpgrade = temp.secondChanceUpgrade;
        magnetUpgrade = temp.magnetUpgrade;
      //  shieldUpgrade = temp.shieldUpgrade;
        coinMultiplierUpgrade = temp.coinMultiplierUpgrade;
        mountDurationUpgrade = temp.mountDurationUpgrade;
        maxPoints = temp.maxPoints;

        //Current match coins have been added, and so are reset.
        currentMatchCoins = 0;

        FindObjectOfType<AudioManager>().Play("Button");
    }
}
