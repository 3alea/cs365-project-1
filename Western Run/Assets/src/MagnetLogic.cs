﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// **********************************************************************
// AUTHOR: Miguel Echeverria
// PURPOSE: Handle all the logic for the magnet power up (picking it
// up, destroying it after certain time, attracting coins etc)
// **********************************************************************


public class MagnetLogic : MonoBehaviour
{
    // ----------Public static variables, these can be upgraded through the shop-----------------

    public static float duration;     // Duration of the magnet power up
    private static MagnetLogic activeObject = null;
    private static bool active = false;
    private float timer;

    // ------------------------------------------------------------------------------------------


    public float radius = 10;       // Radius of the sphere (with origin in player) in which coins will be attracted
    public float moveSpeed = 1;    // Speed of the coins being attracted towards the player

    private bool magnetActive;      // To activate magnet logic
    private GameObject player;      // Reference to the player


    // Start is called before the first frame update
    void Start()
    {
        magnetActive = false;

        //Sets duration according to upgrade level.
        duration = 6 + PlayerStats.magnetLevel * 2;
    }


    // Update is called once per frame
    void LateUpdate()
    {
        // Once the magnet has been activated, attract all nearby coins
        if (magnetActive)
        {
            MagnetEffect();
        }
    }


    void OnDestroy()
    {
        if (active)
        {
            if (activeObject == this)
            {
                //Deactivate.
                active = false;
                activeObject = null;
            }
        }
    }

    // Called once at the beginning of a collision of this object with another one
    // Use this instead of OnCollisionEnter because the coins act only as triggers.
    private void OnTriggerEnter(Collider other)
    {
        // If the power up has collided with the player, start the magnet control coroutine
        // (which will activate the magnet logic, destroy the power up, control its lifetime etc)
        if (other.CompareTag("Player"))
        {
            /*!TODO
             * Powerup pickup SFX
             */

            FindObjectOfType<AudioManager>().Play("PowerUpPick");

            if (!active)
            {
                //Notify that self is active.
                active = true;
                activeObject = this;
                //Start the coroutine.
                StartCoroutine(MagnetControl(other));
            }
            else
            {
                //If active already exists, reset active's timer.
                activeObject.timer = duration;
                //Notify HUD of change.
                GameObject magnet = GameObject.Find("HUD_Magnet");
                magnet.GetComponent<HUDPowerUp>().SetDuration(duration);
            }
        }
    }


    private void MagnetEffect()
    {
        // Get the colliders that are inside a sphere of radius radius
        Collider[] cols = Physics.OverlapSphere(player.transform.position, radius);

        // For each of these colliders that belongs to a coin
        foreach (Collider currentCol in cols)
        {
            if (currentCol.CompareTag("Coin"))
            {
                // Stop the levitation of the coin
                currentCol.GetComponent<CoinLevitation>().enabled = false;

                // Attract the coin
                currentCol.transform.position = Vector3.MoveTowards(currentCol.transform.position, player.transform.position, moveSpeed * Time.deltaTime);
            }
        }
    }


    // Coroutine that is in charge of controlling the lifetime of the magnet power up among other things.
    IEnumerator MagnetControl(Collider playerCol)
    {
        //Disable other components.
        GetComponent<Collider>().enabled = false;
        GetComponent<HazardMovement>().enabled = false;
        GetComponent<CoinLevitation>().enabled = false;
        GetComponent<CoinRotation>().enabled = false;
        //Hide object.
        transform.position = new Vector3(0, -20, 0);

        //Sets timer to duration.
        timer = duration;

        // Activate the logic of the magnet
        magnetActive = true;
        player = playerCol.gameObject;

        GameObject magnet = GameObject.Find("HUD_Magnet");
        magnet.GetComponent<HUDPowerUp>().SetDuration(duration);

        // Wait until the power up has finished
        //Wait for timer.
        while (timer > 0.0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }

        //Deactivate.
        active = false;
        activeObject = null;

        // Destroy the power up
        GameObject.Destroy(gameObject);
    }
}
