﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static float TimeMultOnPause = 0.0f;
    public static bool GameIsPaused = false;

    public GameObject PauseMenuUI;

    public void Start()
    {
        GameIsPaused = false;
        TimeMultOnPause = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
                Resume();
            else
                Pause();
        }
    }

    public void Pause()
    {
        /*!TODO
         * Pause Game SFX
         */


        FindObjectOfType<AudioManager>().Play("Button");

        GameIsPaused = true;
        Time.timeScale = TimeMultOnPause;
        PauseMenuUI.SetActive(true);
    }

    public void Resume()
    {
        /*!TODO
         * Unpause game SFX
         */

        FindObjectOfType<AudioManager>().Play("Button");

        GameIsPaused = false;
        Time.timeScale = 1.0f;
        PauseMenuUI.SetActive(false);
    }

    public void LoadMenu()
    {
        /*!TODO
         * Go to menu SFX (either use unpause game SFX or button click SFX)
         */

        FindObjectOfType<AudioManager>().Play("Button2");


        GameIsPaused = false;
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Menu");   
    }
}
