﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public string levelToLoad;

    public void ChangeLevel()
    {
        GlobalControl inst = GlobalControl.Instance;
        inst.inGame = true;
        SceneManager.LoadScene(levelToLoad);
    }

    public void Exit() {
        Application.Quit();
    }
}
