﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageShower : MonoBehaviour
{
    public GameObject D;
    public GameObject A;
    public GameObject W;
    public GameObject S;
    public GameObject Shield;
    public GameObject Shoe;
    public GameObject Coin;
    public GameObject Win;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //spagetthi
        if (1.0f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 1.1f ) {
            D.SetActive(true);
        }
        else if (4.5f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 4.6f)
        {
            D.SetActive(false);
        }
        else if (6.2f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 6.3f)
        {
            A.SetActive(true);
        }
        else if (10.0f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 10.1f)
        {
            A.SetActive(false);
        }
        else if (11.5f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad <  11.6f)
        {
            W.SetActive(true);
        }
        else if ( 15.0f< Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 15.1f )
        {
            W.SetActive(false);
        }
        else if ( 16.6f< Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 16.7f )
        {
            S.SetActive(true);
        }
        else if (20.6f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 20.7f)
        {
            S.SetActive(false);
        }
        else if (22.0f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 22.1f)
        {
            Shield.SetActive(true);
        }
        else if (27.8f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 27.9f)
        {
            Shield.SetActive(false);
        }
        else if (29.1f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 29.2f)
        {
            Shoe.SetActive(true);
        }
        else if (32.3f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 32.4f)
        {
            Shoe.SetActive(false);
        }
        else if (33.3f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 33.4f)
        {
            Coin.SetActive(true);
        }
        else if (35.3f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 35.4f)
        {
            Coin.SetActive(false);
        }
        else if (37.1f < Time.timeSinceLevelLoad && Time.timeSinceLevelLoad < 37.2f)
        {
            Win.SetActive(true);
        }
        else if (42.0f < Time.timeSinceLevelLoad)
        {
            Application.LoadLevel("Menu");
        }

    }
}
